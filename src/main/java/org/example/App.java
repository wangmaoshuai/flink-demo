package org.example;

import org.apache.flink.runtime.state.storage.FileSystemCheckpointStorage;
import org.apache.flink.streaming.api.CheckpointingMode;
import org.apache.flink.streaming.api.environment.CheckpointConfig;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.EnvironmentSettings;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.TableEnvironment;
import org.apache.flink.table.api.TableResult;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;
import org.apache.flink.types.Row;
import org.apache.flink.util.CloseableIterator;

/**
 * flink sql from kafka to hbase
 */
public class App {
    public static void main(String[] args) throws Exception {
        EnvironmentSettings settings = EnvironmentSettings
                .newInstance()
                .useBlinkPlanner()
                .inStreamingMode()
//                .inBatchMode()
                .build();
        FileSystemCheckpointStorage checkpointStorage = new FileSystemCheckpointStorage("hdfs://tdh03:8020/flink/checkpoints");
//        FileSystemCheckpointStorage checkpointStorage = new FileSystemCheckpointStorage("file://C:\\Users\\wangm\\Documents\\checkpoint");

        StreamExecutionEnvironment fsEnv = StreamExecutionEnvironment.getExecutionEnvironment();
        fsEnv.enableCheckpointing(3000, CheckpointingMode.EXACTLY_ONCE, true);
        fsEnv.getCheckpointConfig().setMinPauseBetweenCheckpoints(500);
        fsEnv.getCheckpointConfig().setCheckpointTimeout(60000);
        fsEnv.getCheckpointConfig().setCheckpointStorage(checkpointStorage);
        fsEnv.getCheckpointConfig().enableExternalizedCheckpoints(CheckpointConfig.ExternalizedCheckpointCleanup.RETAIN_ON_CANCELLATION);
        TableEnvironment tEnv = StreamTableEnvironment.create(fsEnv, settings);
        tEnv.executeSql("CREATE TABLE transactions (\n" +
                "    rowkey  STRING,\n" +
                "    amount      BIGINT,\n" +
                "    log_ts BIGINT,\n" +
                "    log_ts1 AS TO_TIMESTAMP(FROM_UNIXTIME(log_ts / 1000, 'yyyy-MM-dd HH:mm:ss')),\n" +
                "    WATERMARK FOR log_ts1 AS log_ts1  - INTERVAL '5' SECOND\n" +
                ") WITH (\n" +
                "    'connector' = 'kafka',\n" +
                "    'topic'     = 'topic1',\n" +
                "    'scan.startup.mode' = 'earliest-offset',\n" +
                "    'properties.zookeeper.connect' = 'tdh01:2181',\n" +
                "    'properties.bootstrap.servers' = 'tdh01:9092',\n" +
                "    'properties.group.id'='test-consumer-group',\n" +
                "    'csv.field-delimiter'=',',\n" +
                "    'format'    = 'csv'\n" +
                ")");

//        tEnv.executeSql("CREATE TABLE spend_report (\n" +
//                "    account_id VARCHAR,\n" +
//                "    amount     BIGINT\n," +
//                "    log_ts     BIGINT,\n" +
//                "    PRIMARY KEY (account_id) NOT ENFORCED" +
//                ") WITH (\n" +
//                "  'connector'  = 'jdbc',\n" +
//                "  'url'        = 'jdbc:mysql://127.0.0.1:3306/flink_test?serverTimezone=UTC&useUnicode=true&characterEncoding=UTF-8',\n" +
//                "  'table-name' = 'transactions',\n" +
//                "  'driver'     = 'com.mysql.cj.jdbc.Driver',\n" +
//                "  'username'   = 'root',\n" +
//                "  'password'   = '123456a?'\n" +
//                ")");

        tEnv.executeSql("CREATE TABLE spend_report (\n" +
                " rowkey STRING,\n" +
                " transactions_data ROW(amount BIGINT,log_ts BIGINT),\n" +
                " PRIMARY KEY (rowkey) NOT ENFORCED\n" +
                ") WITH (\n" +
                " 'connector' = 'hbase-1.4',\n" +
                " 'table-name' = 'transactions',\n" +
//                "'sink.buffer-flush.max-size'='0'," +
//                "'sink.buffer-flush.max-rows'='0'," +
//                "'sink.buffer-flush.interval'='0'," +
                "'zookeeper.znode.parent'='/hyperbase1'," +
                "'properties.hbase.security.authorization'='false'," +
                "'zookeeper.quorum' = 'tdh03:2181'\n" +
                ")");

        tEnv.executeSql("INSERT INTO spend_report\n" +
                "SELECT rowkey, ROW(amount,log_ts) FROM transactions");

        tEnv.executeSql("SELECT rowkey, transactions_data.amount, transactions_data.log_ts FROM spend_report");
        fsEnv.execute();
//        CloseableIterator<Row> collect = tableResult.collect();
//        while (collect.hasNext()) {
//            System.out.println(collect.next().toString());
//        }
//        tEnv.executeSql("INSERT INTO spend_report\n" +
//                "SELECT rowkey, ROW(amount, log_ts) as transactions_data FROM transactions"
//        ).print();
    }
}
