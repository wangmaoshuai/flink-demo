package org.example.kafka;

import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.connector.kafka.source.KafkaSource;
import org.apache.flink.connector.kafka.source.enumerator.initializer.OffsetsInitializer;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaProducer;
import org.apache.flink.streaming.connectors.kafka.KafkaSerializationSchema;
import org.apache.flink.table.api.EnvironmentSettings;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

/**
 * @author wangm
 */

public class ComputeTimeApp {
    private static final Logger log = LoggerFactory.getLogger(ComputeTimeApp.class);

    public static void main(String[] args) throws Exception {
//        KafkaSource<String> source = KafkaSource.<String>builder()
//                .setBootstrapServers("tdh05:9093")
//                .setTopics("test-timestamp-1")
//                .setGroupId("test-consumer-group")
//                .setStartingOffsets(OffsetsInitializer.earliest())
//                .setValueOnlyDeserializer(new SimpleStringSchema())
//                .build();


        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
//        DataStreamSource<String> kafkaSource = env.fromSource(source, WatermarkStrategy.noWatermarks(), "Kafka Source");
        Properties properties = new Properties();
        properties.setProperty("bootstrap.servers", "tdh01:9092");
        properties.setProperty("group.id", "test-consumer-group");
        DataStream<String> stream = env
                .addSource(new FlinkKafkaConsumer<>("test-timestamp-1", new SimpleStringSchema(), properties));

        SingleOutputStreamOperator<String> streamOperator = stream.map((MapFunction<String, String>) value -> {
            log.info(value);
            String[] split = value.split(",");
            long sendTimestamp = Long.parseLong(split[2]);
            long recTimestamp = System.currentTimeMillis();
            long cost = recTimestamp - sendTimestamp;
            return sendTimestamp + "," + recTimestamp + "," + cost;
        });
        // add producer
        Properties producer = new Properties();
        producer.setProperty("bootstrap.servers", "tdh01:9092");
        KafkaSerializationSchema<String> serializationSchema = (element, timestamp) -> new ProducerRecord<>(
                "test-timestamp-2",
                element.getBytes(StandardCharsets.UTF_8));

        FlinkKafkaProducer<String> myProducer = new FlinkKafkaProducer<>(
                "test-timestamp-2",
                serializationSchema,
                producer,
                FlinkKafkaProducer.Semantic.EXACTLY_ONCE);
        streamOperator.addSink(myProducer);
        env.execute();
    }
}
