package org.example.kafka;

import akka.remote.serialization.StringSerializer;
import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.AdminClientConfig;
import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.PartitionInfo;
import org.apache.kafka.common.errors.TopicExistsException;

import java.time.Duration;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class producer {
    // 创建topic
    public static void createTopic(final String topic,
                                   final Properties cloudConfig) {
        final NewTopic newTopic = new NewTopic(topic, Optional.empty(), Optional.empty());
        try (final AdminClient adminClient = AdminClient.create(cloudConfig)) {
            adminClient.createTopics(Collections.singletonList(newTopic)).all().get();
        } catch (final InterruptedException | ExecutionException e) {
            if (!(e.getCause() instanceof TopicExistsException)) {
                throw new RuntimeException(e);
            }
        }
    }

    public static void main(String[] args) {
//        Properties adminProperty = new Properties();
//        adminProperty.put(
//                AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, "172.29.0.101:9999"
//        );
//        createTopic("topic1", adminProperty);
        //配置信息
        Properties props = new Properties();
        //kafka服务器地址
        //设置数据key和value的序列化处理类
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "tdh01:9092");
//        props.setProperty("zookeeper.connect", "tdh02:2181");
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");
        props.put(ConsumerConfig.GROUP_ID_CONFIG, "test-consumer-group");
        props.put(ProducerConfig.BATCH_SIZE_CONFIG,1);
        props.put(ProducerConfig.ACKS_CONFIG,"all");
        props.put(ProducerConfig.CLIENT_ID_CONFIG, "LoanDataKafkaProducer");
        KafkaConsumer<String, String> consumer = new KafkaConsumer<>(props);
        Map<String, List<PartitionInfo>> topics = consumer.listTopics();
        topics.forEach((k, v) -> System.out.println(k + v.toString()));

        consumer.close();
        //创建生产者实例
        KafkaProducer<String, String> producer = new KafkaProducer<>(props);
        while (true) {
            ProducerRecord<String, String> record = new ProducerRecord<>("test-timestamp-1", UUID.randomUUID().toString(), UUID.randomUUID().toString() + ",100," + System.currentTimeMillis());
            //发送记录
            try{
                RecordMetadata recordMetadata = producer.send(record).get();
                Thread.sleep(1000);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }
}
